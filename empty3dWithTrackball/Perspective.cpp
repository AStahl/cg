#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QHBoxLayout>

#define _USE_MATH_DEFINES
#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>
#include <string> 
#include <iostream>

#include "Perspective.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
: QMainWindow (parent, flags) {
	resize (604, 614);

	// Create a nice frame to put around the OpenGL widget
	QFrame* f = new QFrame (this);
	f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
	f->setLineWidth(2);

	// Create our OpenGL widget
	ogl = new CGView (this,f);

    QMenu *file = new QMenu("&Menu",this);
	file->addAction ("Quit", this, SLOT(close()), Qt::CTRL+Qt::Key_Q);

	menuBar()->addMenu(file);

	// Put the GL widget inside the frame
	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(ogl);
	layout->setMargin(0);
	f->setLayout(layout);

	setCentralWidget(f);

	statusBar()->showMessage("Ready",1000);
}

CGMainWindow::~CGMainWindow () {}


CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent) {
	main = mainwindow;
	fov = 0.4;

    /// Um Keyboard-Events durchzulassen
	setFocusPolicy(Qt::StrongFocus);


}

/// Das kennt Ihr aus der Vorlesung
void CGView::resizeGL(int width, int height) {
  double ratio;

  int W = width;
  int H = height;
  glViewport(0, 0, W, H);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();	

  if (W > H)
  {
    ratio = (double) W / (double) H;
    gluPerspective(180.0*fov,ratio,0.1,10000.0);
  }
  else
  {
    ratio = (double) H / (double) W;
    gluPerspective(180.0*fov,1.0/ratio,0.1,10000.0);
  } 	
  glMatrixMode(GL_MODELVIEW);
}


void CGView::mouseToTrackball (int x, int y, int W, int H, Vector3d &v) {

#if RETINA_DISPLAY
      x *= 2;
      y *= 2;
#endif
	double wx, wy, dist;

	// transform in world coordinates

	// panorama view
	if (W >= H) {
		wx = ( 2.0 / (double)H) * x - ((double)W / (double)H);
		wy = (-2.0 / (double)H) * y + 1.0;
	}

	// portrait view
	else {
		wx = ( 2.0 / (double)W) * x - 1.0;
		wy = (-2.0 / (double)W) * y + ((double)H / (double)W);
	}

	// click inside or outside the sphere
	dist = sqrt(wx*wx + wy*wy);

	// inside
	if (dist <= 1.0) {
		v[0] = wx;
		v[1] = wy;
		v[2] = sqrt(1.0 - wx*wx - wy*wy); // missing z-coordinate
	}

	// outside
	else {
		v[0] = wx;
		v[1] = wy;
		v[2] = 0.0;
		v.normalize(); // jump to next point on the sphere
	}
}

void CGView::trackball (Vector3d u, Vector3d v, Quat4d &q) {

	double qs;
	Vector3d qv, ucv;

	// create the components

	// via given formula (works)
	qs = 1 + u.dot(v);
	qv.cross(u, v);

	// compose and normalize
	q.set(qv[0], qv[1], qv[2], qs);
	q.normalize();
}

void CGView::initializeGL() {

    glEnable(GL_NORMALIZE);
    glEnable(GL_LINE_SMOOTH);

    glClearColor(1, 1, 1, 1);

    qglClearColor(Qt::white);
	zoom = 1.0;
	q_now = Quat4d (.1,.9,.1,.3);
	q_now.normalize();

	center = Vector3d(0.0, 0.0, 0.0);

}


void CGView::paintGL() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    /*	Falls perspektivische Projektion verwendet wird, Szene etwas nach hinten schieben,
        damit sie in der Sichtpyramide liegt. */
     glTranslated(0.0,0.0,-3.0);

    // rotate using quaternions
    Matrix4d M, MT;
    M.makeRotate(q_now);
    MT = M.transpose();
    glMultMatrixd(MT.ptr());

    glScaled(zoom,zoom,zoom);



}

void CGView::mousePressEvent(QMouseEvent *event) {
	mouseToTrackball(event->x(), event->y(), width(), height(), move);
}

void CGView::mouseReleaseEvent(QMouseEvent*) {}

void CGView::wheelEvent(QWheelEvent* event) {
	if (event->delta() < 0) zoom *= 1.2; else zoom *= 1/1.2;
	update();
}

void CGView::mouseMoveEvent(QMouseEvent* event) {
	Vector3d dest;
	Quat4d rotate;

	// get the rotation quaternion
	mouseToTrackball(event->x(), event->y(), width(), height(), dest);
	trackball(move, dest, rotate);

	// rotate global view
	q_now = rotate * q_now;
	q_now.normalize();

	// get ready for next rotation
	mouseToTrackball(event->x(), event->y(), width(), height(), move);

	updateGL();
}

/// Ganz viele Funktionen zum steuern des Fliegers!
void CGView::keyPressEvent( QKeyEvent * event) {

	switch (event->key()) {
//		case Qt::Key_Up    : plane.pitchMe (0.1); break;
	}
		
	updateGL();
}

int main(int argc, char **argv) {

	QApplication app(argc, argv);

	if (!QGLFormat::hasOpenGL()) {
		qWarning ("This system has no OpenGL support. Exiting.");
		return 1;
	}

	CGMainWindow *w = new CGMainWindow(NULL);

	w->show();

	return app.exec();
}
