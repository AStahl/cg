#ifndef PERSPECTIVE_H
#define PERSPECTIVE_H

#include <QMainWindow>
#include <QGLWidget>
#include <QGridLayout>
#include <QFrame>
#include <QActionGroup>
#include <vector>
#include <iostream>

#include "vecmath.h"

#ifndef VECMATH_VERSION
#  error "vecmath-library might be out-of-date. Please use newer version"
#endif

#if _MSC_VER
	#include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
  #define RETINA_DISPLAY 0
#else
	#include <GL/glu.h>
#endif

class CGView;

class CGMainWindow : public QMainWindow {
	Q_OBJECT

public:

	CGMainWindow (QWidget* parent = 0, Qt::WindowFlags flags = Qt::Window);
	~CGMainWindow ();

	QActionGroup *projectionMode;

public slots:

//	void loadPolyhedron();

private:

	CGView *ogl;	

};

class CGView : public QGLWidget {
	Q_OBJECT

public:

	CGView(CGMainWindow*,QWidget*);
	void initializeGL();
	
	/** transforms the picture coords (x,y,z) to world coords by 
		inverting the projection and modelview matrix (as it it is 
		after invocation of paintGL) and stores the result in v */
	void worldCoord(int x, int y, int z, Vector3d &v);

  Vector3d min, max, center, move;	// min, max and center of the coords of the loaded model
	Quat4d q_now;
  double zoom;
	double phi,theta;
	std::vector<Vector3d> coord;			// the coords of the loaded model
	std::vector<std::vector<int> > ifs;		// the faces of the loaded model, ifs[i] contains the indices of the i-th face
	
protected:

	void paintGL();
	void resizeGL(int,int);

	void mouseMoveEvent(QMouseEvent*);
	void mousePressEvent(QMouseEvent*);
	void mouseReleaseEvent(QMouseEvent*);
	void wheelEvent(QWheelEvent*);
	void keyPressEvent (QKeyEvent * event);

	CGMainWindow *main;
    int oldX,oldY;

private:

    void mouseToTrackball(int, int, int, int, Vector3d&);
    void trackball (Vector3d, Vector3d, Quat4d&);
		// Field of Interest f"ur gluPerspective!
        double fov;// = 0.4;

		/// Wireframe oder Filled?
        bool wire;// = false;

    void drawTorus(int p_num, int t_num);
    void makeTorus(double p, double t, double R, double r, std::vector<std::vector<Vector3d> > *torus);


public slots:

	void updateProjectionMode() {
		resizeGL(width(),height());
		updateGL();
	}

};

#endif
