TEMPLATE = app
TARGET = Bezier
QT += gui opengl
CONFIG += console
HEADERS += *.h
SOURCES += *.cpp 

macx: QMAKE_MAC_SDK =  macosx10.11

unix:!macx {LIBS += -lGLU}
