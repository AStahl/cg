#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QHBoxLayout>
#include "Bezier.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>

#ifdef max
#undef max
#endif

/// Testfunktion, falls OpenGL nicht richtig mitspielt. Einfach am Ende jeder 
/// Funktion einfügen, als Argument den Namen der Funktion in Anführungszeichen.
void glPrintErrors(const char* as_Function)
{
  GLenum lr_Error = GL_NO_ERROR;
  bool lb_ErrorFound = false;
  int li_ErrorCount = 5;
  do
    {
      lr_Error = glGetError();
      if (lr_Error != GL_NO_ERROR)
        {
          lb_ErrorFound = true;
          li_ErrorCount--;
          char* ls_ErrorString = (char*)gluErrorString(lr_Error);
          std::cerr << "OpenGL Error (" << as_Function << "): " << ls_ErrorString << std::endl;
        }
      if (li_ErrorCount == 0)
        {
          std::cerr << "OPENGL :: ERROR Too many errors!" << std::endl;
          break;
        }
      } while (lr_Error != GL_NO_ERROR);
    if (lb_ErrorFound) exit(0);
}


CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
: QMainWindow (parent, flags) {
	resize (604, 614);

	// Create a nice frame to put around the OpenGL widget
	QFrame* f = new QFrame (this);
	f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
	f->setLineWidth(2);

	// Create our OpenGL widget
	ogl = new CGView (this,f);

	QMenu *options = new QMenu("&Options",this);
	QAction* action;
	action = options->addAction ("Show Bezier", ogl, SLOT(toggleBezier()), Qt::CTRL+Qt::Key_B);
	action->setCheckable (true);
	action->setChecked (ogl->show_bezier);
    action = options->addAction ("Show Own", ogl, SLOT(toggleOwn()), Qt::CTRL+Qt::Key_O);
    action->setCheckable (true);
    action->setChecked (ogl->show_own);


	menuBar()->addMenu(options);

	// Put the GL widget inside the frame
	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(ogl);
	layout->setMargin(0);
	f->setLayout(layout);

	setCentralWidget(f);

	statusBar()->showMessage("Ready",1000);
}

CGMainWindow::~CGMainWindow () { }

void CGMainWindow::keyPressEvent(QKeyEvent* event) {
	switch(event->key()) {
	case Qt::Key_I: std::cout << "I" << std::flush; break;
	case Qt::Key_M: std::cout << "M" << std::flush; break;
	}

	ogl->updateGL();
}

CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent) {
	main = mainwindow;

	centerX = centerY = 0;

	show_bezier = false;

	control_points.push_back (Vector3d(-.7,.7,0));
	control_points.push_back (Vector3d(-.7,-.7,0));
	control_points.push_back (Vector3d(.7,.7,0));
	control_points.push_back (Vector3d(.7,-.7,0));

}

void CGView::initializeGL() 
{
	qglClearColor(Qt::white);
	zoom = 1.0;
}

/// Aufgabe 1, (a)
void CGView::draw_bezier_openGL (const std::vector<Vector3d> &l)
{
    int numberOfDrawnSegments = 100;

    glMap1d(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, l.size(), l[0].ptr());
    glEnable(GL_MAP1_VERTEX_3);
    glMapGrid1d(numberOfDrawnSegments, 0.0, 1.0);
    glEvalMesh1(GL_LINE, 0, numberOfDrawnSegments);

    //merkwürdiges verhalten wenn man punkte hinzufügt, keine idee warum...
    //Es scheint als würde die Punkteliste nicht aktualisiert...
    //Es geht immer mit exakt 4 Punkten...

}

/// Aufgabe 1, (a)//eigentlich b)
Vector3d CGView::de_casteljau(double t0, const std::vector<Vector3d> &l)
{
    /// ADD YOUR CODE

    std::vector<Vector3d> p_neu, p_alt;
    Vector3d dummy;

    int n = l.size();

    for (int i = 0; i <= n; i++) {
        p_alt.push_back(l[i]);
    }

    do {
        for (int i = 0; i < n; i++) {
            dummy = p_alt[i]*(1-t0) + p_alt[i+1]*t0;
            p_neu.push_back(dummy);
        }
        p_alt.clear();
        p_alt.swap(p_neu);
        n--;
    } while (n > 0);
    return p_neu[0];
}

/// Aufgabe 1, (b)//eigentlich c)
void CGView::draw_bezier_casteljau (const std::vector<Vector3d> &l)
{
	/// ADD YOUR CODE

    glBegin(GL_LINE_STRIP);
    for (double t = 0; t <= 1; t+=0.01) {
        glVertex3dv(de_casteljau(t, l).ptr());
    }
    glEnd();
}




void CGView::paintGL() {
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glScaled(zoom,zoom,1.0);
	glTranslated(-centerX,-centerY,0.0);

	glClear(GL_COLOR_BUFFER_BIT);

	glColor3d (0,0,1);
	glPointSize (1.0);
	glBegin(GL_LINE_STRIP);
	for(int i=0;i<(int) control_points.size();i++)
				glVertex2dv(control_points[i].ptr());
	glEnd();

	glColor3d (1,0,0);
	glPointSize (3.0);
	glBegin(GL_POINTS);

    bool reset = false;

    for(int i=0;i<(int) control_points.size();i++) {
        if (i == gewahlteListenStelle) {
            glColor3d (0,1,0);
            glPointSize(100.0);
            reset = true;
        }
				glVertex2dv(control_points[i].ptr());
        if (reset) {
            glColor3d (1,0,0);
            glPointSize (3.0);
            reset = false;
        }
    }

	glEnd();



	if (show_bezier)
		{
			glColor3d (0,0,0);
			glPointSize (2.0);
            draw_bezier_openGL (control_points);
		}
    if (show_own) {
        glColor3d (.8,0,0);
        glPointSize (2.0);
        draw_bezier_casteljau(control_points);
    }
}

void CGView::resizeGL(int width, int height) {
	glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width > height) {
		double ratio = width/(double) height;
		gluOrtho2D(-ratio,ratio,-1.0,1.0);
	}
	else {
		double ratio = height/(double) width;
		gluOrtho2D(-1.0,1.0,-ratio,ratio);
	}
	glMatrixMode (GL_MODELVIEW);
}

void CGView::worldCoord(int x, int y, double &dx, double &dy) {
	if (width() > height()) {
		dx = (2.0*x-width())/height();
		dy = 1.0-2.0*y/(double) height();
	} else {
		dx = 2.0*x/(double) width()-1.0;
		dy = (height()-2.0*y)/width();
	}
	dx /= zoom;
	dy /= zoom;
}

void CGView::mousePressEvent(QMouseEvent *event) {
	double dx, dy;
	worldCoord(event->x(),event->y(),dx,dy);
    std::cout << "Mouse pressed at (" << dx << "," << dy <<")" << std::endl;

    Vector3d p;
    for (int i = 0; i < control_points.size(); i++) {
        p = control_points[i];
        if (std::abs(p.x()-dx) < epsilon && std::abs(p.y()-dy) < epsilon) {
            gewahlteListenStelle = i;
            if (event->button() == Qt::RightButton) {
                control_points.erase(control_points.begin()+i);
            }
        }

    }

    if (gewahlteListenStelle < 0) {
        Vector3d a, b, p = Vector3d(dx, dy, 0);
        double dist;
        std::vector<Vector3d> neu_points;
        for (int i = 0; i < control_points.size()-1; i++) {
            a = control_points[i];
            b = control_points[i+1];
            dist = ((b-a)%(p-a)).length() / (p-a).length();
            if (dist < epsilon) {
                for (int j = 0; j <= i; j++) neu_points.push_back(control_points[j]);
                neu_points.push_back(p);
                for (int j = i+1; j < control_points.size(); j++) neu_points.push_back(control_points[j]);
                control_points.clear();
                control_points = neu_points;
                break;
            }
        }
    }
    update();

}

void CGView::mouseReleaseEvent (QMouseEvent* event) {
	std::cout << "Mouse released" << std::endl;
    gewahlteListenStelle = -1;
    update();
}

void CGView::wheelEvent(QWheelEvent* event) {
	if (event->delta() < 0) zoom *= 1.1; else zoom *= 1/1.1;
	update();
}

void CGView::mouseMoveEvent (QMouseEvent* event) {
    //std::cout << "Mouse moved" << std::endl;

	int x = event->x();
	int y = event->y();
    if (event->button() == Qt::LeftButton) {//geht nicht, da button = 0 => no button
		updateGL();
    }

    if (gewahlteListenStelle > -1) {
        double dx, dy;
        worldCoord(x, y, dx, dy);
        control_points[gewahlteListenStelle] = Vector3d(dx, dy, 0);
    }
    update();

}

int main (int argc, char **argv) {
	QApplication app(argc, argv);

	if (!QGLFormat::hasOpenGL()) {
		qWarning ("This system has no OpenGL support. Exiting.");
		return 1;
	}

	CGMainWindow *w = new CGMainWindow(NULL);

	w->show();

	return app.exec();
}

