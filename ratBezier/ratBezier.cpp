#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QHBoxLayout>

#define _USE_MATH_DEFINES
#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>
#include <string> 
#include <iostream>

#include "ratBezier.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
: QMainWindow (parent, flags) {
	resize (604, 614);

	// Create a nice frame to put around the OpenGL widget
	QFrame* f = new QFrame (this);
	f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
	f->setLineWidth(2);

	// Create our OpenGL widget
	ogl = new CGView (this,f);

    QMenu *file = new QMenu("&Menu",this);
	file->addAction ("Quit", this, SLOT(close()), Qt::CTRL+Qt::Key_Q);

	menuBar()->addMenu(file);

	// Put the GL widget inside the frame
	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(ogl);
	layout->setMargin(0);
	f->setLayout(layout);

	setCentralWidget(f);

	statusBar()->showMessage("Ready",1000);
}

CGMainWindow::~CGMainWindow () {}


CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent) {
	main = mainwindow;
	fov = 0.4;

    /// Um Keyboard-Events durchzulassen
    setFocusPolicy(Qt::StrongFocus);

    resetA2();

}

/// Das kennt Ihr aus der Vorlesung
void CGView::resizeGL(int width, int height) {
  double ratio;

  int W = width;
  int H = height;
  glViewport(0, 0, W, H);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();	

  if (W > H)
  {
    ratio = (double) W / (double) H;
    gluPerspective(180.0*fov,ratio,0.1,10000.0);
  }
  else
  {
    ratio = (double) H / (double) W;
    gluPerspective(180.0*fov,1.0/ratio,0.1,10000.0);
  } 	
  glMatrixMode(GL_MODELVIEW);
}

void CGView::resetA1()
{
    controlpts.clear();
    controlpts.push_back(Vector4d(1, 0, 1, 1));
    controlpts.push_back(Vector4d(1, 1, 1, 1)/sqrt(2));
    controlpts.push_back(Vector4d(0, 1, 1, 1));
}

void CGView::resetA2()
{
    controlpts.clear();
    controlpts.push_back(Vector4d(-1, -1, 2, 2));
    controlpts.push_back(Vector4d(-1, 1, 1.5, 1.5));
    controlpts.push_back(Vector4d(1, -1, 2, 2));
    controlpts.push_back(Vector4d(1, 1, 1, 1));
}


void CGView::mouseToTrackball (int x, int y, int W, int H, Vector3d &v) {

#if RETINA_DISPLAY
      x *= 2;
      y *= 2;
#endif
	double wx, wy, dist;

	// transform in world coordinates

	// panorama view
	if (W >= H) {
		wx = ( 2.0 / (double)H) * x - ((double)W / (double)H);
		wy = (-2.0 / (double)H) * y + 1.0;
	}

	// portrait view
	else {
		wx = ( 2.0 / (double)W) * x - 1.0;
		wy = (-2.0 / (double)W) * y + ((double)H / (double)W);
	}

	// click inside or outside the sphere
	dist = sqrt(wx*wx + wy*wy);

	// inside
	if (dist <= 1.0) {
		v[0] = wx;
		v[1] = wy;
		v[2] = sqrt(1.0 - wx*wx - wy*wy); // missing z-coordinate
	}

	// outside
	else {
		v[0] = wx;
		v[1] = wy;
		v[2] = 0.0;
		v.normalize(); // jump to next point on the sphere
	}
}

void CGView::trackball (Vector3d u, Vector3d v, Quat4d &q) {

	double qs;
	Vector3d qv, ucv;

	// create the components

	// via given formula (works)
	qs = 1 + u.dot(v);
	qv.cross(u, v);

	// compose and normalize
	q.set(qv[0], qv[1], qv[2], qs);
	q.normalize();
}

void CGView::initializeGL() {

    glEnable(GL_NORMALIZE);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA );

    glClearColor(1, 1, 1, 1);

    qglClearColor(Qt::white);
	zoom = 1.0;
	q_now = Quat4d (.1,.9,.1,.3);
	q_now.normalize();

	center = Vector3d(0.0, 0.0, 0.0);

}


void CGView::paintGL() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    /*	Falls perspektivische Projektion verwendet wird, Szene etwas nach hinten schieben,
        damit sie in der Sichtpyramide liegt. */
     glTranslated(0.0,0.0,-3.0);

    // rotate using quaternions
    Matrix4d M, MT;
    M.makeRotate(q_now);
    MT = M.transpose();
    glMultMatrixd(MT.ptr());

    glScaled(zoom,zoom,zoom);

    //Koordinatensystem zur Hilfe
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glLineWidth(1);
    glColor3d(1.,0.,0.);
    glBegin(GL_LINE_STRIP);
    glVertex3d(0,0,0);
    glVertex3d(2,0,0);
    glEnd();
    glColor3d(0,1.,0);
    glBegin(GL_LINE_STRIP);
    glVertex3d(0,0,0);
    glVertex3d(0,2,0);
    glEnd();
    glColor3d(0,0,1.);
    glBegin(GL_LINE_STRIP);
    glVertex3d(0,0,0);
    glVertex3d(0,0,2);
    glEnd();

    //0-ebene
    glColor3d(.8,.8,.8);
    glBegin(GL_LINE_STRIP);
    glVertex3d(-1,-1,0);
    glVertex3d(1,-1,0);
    glVertex3d(1,1,0);
    glVertex3d(-1,1,0);
    glVertex3d(-1,-1,0);
    glEnd();

    //1-ebene zeichnen
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glColor4d(.8,.8,.8,.5);
    glBegin(GL_QUADS);
        glVertex3d(-1,-1,1);
        glVertex3d(-1,1,1);
        glVertex3d(1,1,1);
        glVertex3d(1,-1,1);
    glEnd();

    //Controlpunkte zeichnen
    glColor3d(0,0,.8);
    glPointSize(10);
    glBegin(GL_POINTS);
    for (unsigned int i=0; i<controlpts.size(); i++) {
        if (i == gewaehlterPunkt) glColor3d(0,.8,0);
        glVertex3dv(controlpts[i].ptr());
        if (i == gewaehlterPunkt) glColor3d(0,0,.8);
    }
    glEnd();

    //Controlpolygon 3d zeichnen
    glColor3d(.2,.2,.2);
    glBegin(GL_LINE_STRIP);
    for (unsigned int i=0; i<controlpts.size(); i++) {
        glVertex3dv(controlpts[i].ptr());
    }
    glEnd();

    //Controlpolygon 2d zeichnen
    glBegin(GL_LINE_STRIP);
    double z;
    for (unsigned int i=0; i<controlpts.size(); i++) {
        z = controlpts[i].z();
        glVertex3d(controlpts[i].x()/z, controlpts[i].y()/z, 1.01);
    }
    glEnd();



    glLineWidth(2);
    int numberOfDrawnSegments = 100;

    //v3 zeichnen
    glColor3d(1,0,0);
    glEnable(GL_MAP1_VERTEX_3);
    glMap1d(GL_MAP1_VERTEX_3, 0.0, 1.0, 4, controlpts.size(), controlpts[0].ptr());
    glMapGrid1d(numberOfDrawnSegments, 0.0, 1.0);
    glEvalMesh1(GL_LINE, 0, numberOfDrawnSegments);
    glDisable(GL_MAP1_VERTEX_3);


    //4d aus 3d errechnen
    //TODO hier stimmt offensichtlich was nicht...
    std::vector<double> ctrlPts4d;
    for (unsigned int i=0; i < controlpts.size(); i++) {
        ctrlPts4d.push_back(controlpts[i].x());
        ctrlPts4d.push_back(controlpts[i].y());
        ctrlPts4d.push_back(controlpts[i].z());
        ctrlPts4d.push_back(controlpts[i].z());
    }
    //v4 zeichnen
    glColor3d(0,1,0);
    glEnable(GL_MAP1_VERTEX_4);
    glMap1d(GL_MAP1_VERTEX_4, 0.0, 1.0, 4, controlpts.size(), ctrlPts4d.data());
    glMapGrid1d(numberOfDrawnSegments, 0.0, 1.0);
    glEvalMesh1(GL_LINE, 0, numberOfDrawnSegments);
    glDisable(GL_MAP1_VERTEX_4);

    //hier geht was schief, der 0-Punkt scheint in das Grid aufgenommen zu werden...


    //TODO
//beim ändern der gewichte geht die kurve, die in der ebene bleiben sollte, vom kontrollpunkt weg. die andere, die eh nicht geht, ändert sich gar nicht. Ich bin verwirrt...

}

void CGView::mousePressEvent(QMouseEvent *event) {
	mouseToTrackball(event->x(), event->y(), width(), height(), move);
}

void CGView::mouseReleaseEvent(QMouseEvent*) {}

void CGView::wheelEvent(QWheelEvent* event) {
	if (event->delta() < 0) zoom *= 1.2; else zoom *= 1/1.2;
	update();
}

void CGView::mouseMoveEvent(QMouseEvent* event) {
	Vector3d dest;
	Quat4d rotate;

	// get the rotation quaternion
	mouseToTrackball(event->x(), event->y(), width(), height(), dest);
	trackball(move, dest, rotate);

	// rotate global view
	q_now = rotate * q_now;
	q_now.normalize();

	// get ready for next rotation
	mouseToTrackball(event->x(), event->y(), width(), height(), move);

	updateGL();
}

void CGView::keyPressEvent( QKeyEvent * event) {
    if(event->modifiers()&Qt::ControlModifier){
        switch(event->key()){
        case Qt::Key_Plus:
            if(gewaehlterPunkt==-1){
                //insert before first
                controlpts.insert(controlpts.begin(),controlpts[0]*1.5-controlpts[1]*0.5);
            } else if(gewaehlterPunkt == controlpts.size()-1){
                //insert after last
                controlpts.insert(controlpts.end(),controlpts[controlpts.size()-1]*1.5-controlpts[controlpts.size()-2]*0.5);
            } else{
                //insert after gewählt
                controlpts.insert(controlpts.begin()+gewaehlterPunkt+1,controlpts[gewaehlterPunkt]*0.5+controlpts[gewaehlterPunkt+1]*0.5);
            }
            break;
           case Qt::Key_Minus:
            if(controlpts.size()>2)
                controlpts.erase(controlpts.begin()+gewaehlterPunkt);
            //delete a Point
            break;
        case Qt::Key_1:
            resetA1();
            break;
        case Qt::Key_2:
            resetA2();
            break;
        }
    }
    else{
    switch (event->key()) {
    case Qt::Key_Space :
        if (gewaehlterPunkt==controlpts.size())
            gewaehlterPunkt = 0;
        else gewaehlterPunkt++;
        break;
    case Qt::Key_Plus :
        if (gewaehlterPunkt!=-1)
            controlpts[gewaehlterPunkt]*=1.1;
        break;
    case Qt::Key_Minus :
        if (gewaehlterPunkt!=-1)
            controlpts[gewaehlterPunkt]/=1.1;
        break;
    case Qt::Key_W :
        if (gewaehlterPunkt!=-1)
            controlpts[gewaehlterPunkt][0]+=.1;
        break;
    case Qt::Key_S :
        if (gewaehlterPunkt!=-1)
            controlpts[gewaehlterPunkt][0]-=.1;
        break;
    case Qt::Key_A :
        if (gewaehlterPunkt!=-1)
            controlpts[gewaehlterPunkt][1]+=.1;
        break;
    case Qt::Key_D :
        if (gewaehlterPunkt!=-1)
            controlpts[gewaehlterPunkt][1]-=.1;
        break;
    }
    }
    //for (int i = 0; i < weights.size(); i++) std::cout << weights[i];
    //std::cout << std::endl;
    updateGL();
}

int main(int argc, char **argv) {

	QApplication app(argc, argv);

	if (!QGLFormat::hasOpenGL()) {
		qWarning ("This system has no OpenGL support. Exiting.");
		return 1;
	}

	CGMainWindow *w = new CGMainWindow(NULL);

	w->show();

	return app.exec();
}
