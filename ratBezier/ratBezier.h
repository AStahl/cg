#ifndef PERSPECTIVE_H
#define PERSPECTIVE_H

#include <QMainWindow>
#include <QGLWidget>
#include <QGridLayout>
#include <QFrame>
#include <QActionGroup>
#include <vector>
#include <iostream>

#include "vecmath.h"

#ifndef VECMATH_VERSION
#  error "vecmath-library might be out-of-date. Please use newer version"
#endif

#if _MSC_VER
	#include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
  #define RETINA_DISPLAY 0
#else
	#include <GL/glu.h>
#endif

class CGView;

class CGMainWindow : public QMainWindow {
	Q_OBJECT

public:

	CGMainWindow (QWidget* parent = 0, Qt::WindowFlags flags = Qt::Window);
	~CGMainWindow ();

	QActionGroup *projectionMode;

public slots:

private:

	CGView *ogl;	

};

class CGView : public QGLWidget {
	Q_OBJECT

public:

	CGView(CGMainWindow*,QWidget*);
	void initializeGL();

    Vector3d min, max, center, move;	// min, max and center of the coords of the loaded model
	Quat4d q_now;
    double zoom;
	double phi,theta;
    std::vector<Vector4d> controlpts;
	
protected:

	void paintGL();
	void resizeGL(int,int);
    void resetA1();
    void resetA2();

	void mouseMoveEvent(QMouseEvent*);
	void mousePressEvent(QMouseEvent*);
	void mouseReleaseEvent(QMouseEvent*);
	void wheelEvent(QWheelEvent*);
	void keyPressEvent (QKeyEvent * event);

	CGMainWindow *main;
    int oldX,oldY;
    int gewaehlterPunkt = -1;

private:

    void mouseToTrackball(int, int, int, int, Vector3d&);
    void trackball (Vector3d, Vector3d, Quat4d&);
    // Field of Interest f"ur gluPerspective!
    double fov;// = 0.4;
    double maxHeight;// = 1;
		
public slots:

	void updateProjectionMode() {
		resizeGL(width(),height());
		updateGL();
	}

};

#endif
