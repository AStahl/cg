#ifndef MYPOLYGON_H
#define MYPOLYGON_H

#include "vecmath.h"
#include <vector>
#include <algorithm>
#include <math.h>
#include <iostream>

#include <QtOpenGL>


class MyPlane
{

public:

    MyPlane ()
    {
        M.makeIdentity();
    };

    void setPoints (std::vector< Vector3d> l)
    {
        points_.clear ();
        points_.insert (points_.end(), l.begin(), l.end());
        set_into_mass_center ();
    };

    ///Malt das Polygon
    void draw(Vector3d color = Vector3d(0,0,0),  bool fill=false)
    {
        glPushMatrix();

        Matrix4d MT = M.transpose();
        glMultMatrixd (MT.ptr());

        glColor3dv (color.ptr());
        if (fill)
        {
            glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
            glBegin(GL_POLYGON);
        }
        else glBegin(GL_LINE_LOOP);
        for (unsigned int i=0;i<points_.size();i++)
            glVertex3d (points_[i].x(), points_[i].y(), points_[i].z());
        glEnd();

        glPopMatrix();
    };

    void placeMe (Vector3d p)
    {
        M.setTrans(p);
    };

    void flyMe (double step)
    {

        Vector3d nose = Matrix4d::transform3x3(M,Vector3d(1,0,0));
        M.setTrans(M.getTrans() + nose * step);


    };

    void rollMe (double alpha)
    {
        ///ADD YOUR CODE HERE


    };

    void yawMe (double alpha)
    {
        ///ADD YOUR CODE HERE

    };

    void pitchMe (double alpha)
    {
        ///ADD YOUR CODE HERE

        Vector3d pitchAxis = Matrix4d::transform3x3(M,Vector3d(0,0,1));

        //M.setRotate(M.getRotate()); //Wie Rotiere ich ne Matrix?

    };


    int size () {return points_.size();}



private:

    void set_into_mass_center ()
    {
        ///ADD YOUR CODE HERE
        Vector3d massCenter;
        //Massencentrum Berechnen (Sum(Points)/Size)
        for (int i = 0; i < size(); i++) {
            massCenter += points_[i];
        }
        massCenter = massCenter / size();
        //Alle Points verschieben
        for (int i = 0; i < size(); i++) {
            points_[i] -= massCenter;
        }
    }

    std::vector<Vector3d> points_;
    Matrix4d M;

};

#endif
