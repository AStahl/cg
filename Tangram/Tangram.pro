TEMPLATE = app
TARGET = Tamgram
QT += gui opengl
CONFIG += console
HEADERS += *.h
SOURCES += *.cpp
QMAKE_CXXFLAGS = -std=c++11

macx: QMAKE_MAC_SDK =  macosx10.11

unix:!macx {LIBS += -lGLU}
