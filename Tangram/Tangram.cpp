#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QHBoxLayout>
#include "Tangram.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>
#include <tuple>
#include <assert.h>

#ifdef max
#undef max
#endif

CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
    : QMainWindow (parent, flags) {
    resize (604, 614);

    // Create a menu
    QMenu *file = new QMenu("&File",this);
    file->addAction ("Load polygon", this, SLOT(loadPolygon()), Qt::CTRL+Qt::Key_L);
    file->addAction ("Duplicate active polygon", this, SLOT(duplicatePolygon()), Qt::CTRL+Qt::Key_D);
    file->addAction ("Quit", qApp, SLOT(quit()), Qt::CTRL+Qt::Key_Q);
    menuBar()->addMenu(file);

    // Create a nice frame to put around the OpenGL widget
    QFrame* f = new QFrame (this);
    f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
    f->setLineWidth(2);

    // Create our OpenGL widget
    ogl = new CGView (this,f);

    // Put the GL widget inside the frame
    QHBoxLayout* layout = new QHBoxLayout();
    layout->addWidget(ogl);
    layout->setMargin(0);
    f->setLayout(layout);

    setCentralWidget(f);

    statusBar()->showMessage("Ready",1000);
}

CGMainWindow::~CGMainWindow () { }


void CGMainWindow::loadPolygon() {
    QString fn = QFileDialog::getOpenFileName(this, "Load polygon ...", QString(), "POL files (*.pol)" );

    if (fn.isEmpty()) return;
    statusBar()->showMessage ("Loading polygon ...");
    std::ifstream file(fn.toLatin1());
    double x,y;
    int m,n;

    ogl->minX = ogl->minY = std::numeric_limits<double>::max();
    ogl->maxX = ogl->maxY = -ogl->minX;

    file >> n;
    ogl->poly.resize(n);
    for(int i=0;i<n;i++) {
        file >> m;
        ogl->poly[i].resize(3*m);
        for(int j=0;j<m;j++) {
            file >> x >> y;
            if (x < ogl->minX) ogl->minX = x;
            else if (x > ogl->maxX) ogl->maxX = x;
            if (y < ogl->minY) ogl->minY = y;
            else if (y > ogl->maxY) ogl->maxY = y;
            ogl->poly[i][3*j+0] = x;
            ogl->poly[i][3*j+1] = y;
            ogl->poly[i][3*j+2] = 0.0;
        }
    }
    file.close();

    std::cout << "minX = " << ogl->minX << std::endl;
    std::cout << "maxX = " << ogl->maxX << std::endl;
    std::cout << "minY = " << ogl->minY << std::endl;
    std::cout << "maxY = " << ogl->maxY << std::endl;

    ogl->zoom = 2.0/std::max(ogl->maxX-ogl->minX,ogl->maxY-ogl->minY);

    // this is center of the scene
    ogl->centerX = (ogl->minX+ogl->maxX)/2;
    ogl->centerY = (ogl->minY+ogl->maxY)/2;

    ogl->i_picked = 0;

    // center of each polygon
    ogl->center.resize(n);
    std::vector<double> c;
    c.resize(2);
    for(int i=0;i<n;i++) {
        c[0] = 0;c[1] = 0;
        for(int j=0;j<m-1;j++) { // m-1 because loop
            double x = ogl->poly[i][3*j+0];
            double y = ogl->poly[i][3*j+1];
            c[0]+=x;
            c[1]+=y;
        }
        c[0]/=m-1;
        c[1]/=m-1;

        ogl->center[i].resize(3);
        ogl->center[i][0] = c[0];
        ogl->center[i][1] = c[1];
        ogl->center[i][2] = 0.0;

    }

    // inital transformation == 0
    ogl->trans.resize(n);
    for(int i=0;i<n;i++) {
        ogl->trans[i].resize(3);
        ogl->trans[i][0] = 0; // x
        ogl->trans[i][1] = 0; // y
        ogl->trans[i][2] = 0; // phi
    }

    ogl->updateGL();
    statusBar()->showMessage ("Loading polygon done.",3000);
    std::cout << "Loading done " << std::endl;

}

void CGMainWindow::duplicatePolygon() {
    //duplicates the picked polygon
    int i=ogl->i_picked;
    if (i>ogl->poly.size()-1) return;
    if (i<0) return;

    int N = ogl->poly.size();
    ogl->poly.resize(N+1);
    ogl->poly[N].resize(ogl->poly[i].size());
    for (int j=0;j<ogl->poly[i].size();j++)
        ogl->poly[N][j] = ogl->poly[i][j];

    N = ogl->center.size();
    ogl->center.resize(N+1);
    ogl->center[N].resize(2);
    ogl->center[N][0] = ogl->center[i][0];
    ogl->center[N][1] = ogl->center[i][1];

    N = ogl->trans.size();
    ogl->trans.resize(N+1);
    ogl->trans[N].resize(3);
    ogl->trans[N][0] = ogl->trans[i][0]+0;
    ogl->trans[N][1] = ogl->trans[i][1]+0;
    ogl->trans[N][2] = ogl->trans[i][2]+0;
    ogl->i_picked = N;
    std::cout << "number of polygons:" << ogl->trans.size() << std::endl;
    ogl->updateGL();
}

void CGMainWindow::keyPressEvent(QKeyEvent* event) {
    switch(event->key()) {
    case Qt::Key_I: std::cout << "I" << std::flush; break;
    case Qt::Key_M: std::cout << "M" << std::flush; break;
    case Qt::Key_Q: ogl->trans[ogl->i_picked][2]+=5; break;
    case Qt::Key_W: ogl->trans[ogl->i_picked][2]-=5; break;
    }

    ogl->updateGL();
}

CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent) {
    main = mainwindow;
}

void CGView::initializeGL() {
    qglClearColor(Qt::white);
    zoom = 1.0;
    fillMode = 0;

    tobj = gluNewTess();
#ifdef _MSC_VER
    gluTessCallback(tobj,(GLenum)GLU_TESS_BEGIN, (void(APIENTRY*)()) glBegin);
    gluTessCallback(tobj,(GLenum)GLU_TESS_VERTEX,(void(APIENTRY*)()) glVertex3dv);
    gluTessCallback(tobj,(GLenum)GLU_TESS_END,   (void(APIENTRY*)()) glEnd);
#elif __MINGW32__   //das hier hinzufügen (wie in Lizard)
    gluTessCallback(tobj,(GLenum)GLU_TESS_BEGIN, (void(APIENTRY*)()) glBegin);
    gluTessCallback(tobj,(GLenum)GLU_TESS_VERTEX,(void(APIENTRY*)()) glVertex3dv);
    gluTessCallback(tobj,(GLenum)GLU_TESS_END,   (void(APIENTRY*)()) glEnd);    // bis hier
#else
    gluTessCallback(tobj,(GLenum)GLU_TESS_BEGIN, (void(*)()) glBegin);
    gluTessCallback(tobj,(GLenum)GLU_TESS_VERTEX,(void(*)()) glVertex3dv);
    gluTessCallback(tobj,(GLenum)GLU_TESS_END,   (void(*)()) glEnd);
#endif
    gluTessProperty(tobj,GLU_TESS_BOUNDARY_ONLY,GL_FALSE);
}

void CGView::paintGL() {
    glMatrixMode(GL_MODELVIEW);

    glClear(GL_COLOR_BUFFER_BIT);

    for(int i=0;i<(int) poly.size();i++) {

        // View
        glLoadIdentity();
        glScaled(zoom,zoom,1.0);
        glTranslated(-centerX,-centerY,0.0);

        // Model
        glTranslated(trans[i][0],trans[i][1],0);
        glTranslated(center[i][0],center[i][1],0);
        glRotated(trans[i][2],0,0,1);
        glTranslated(-center[i][0],-center[i][1],0);
        glColor3f(0.0f,1.0f,0.0f);
        if (i == i_picked)
            glColor3f(1.0f,0.0f,0.0f);

        // draw ith polygons interior
        gluTessBeginPolygon(tobj,NULL);
        gluTessBeginContour(tobj);
        for(int j=0;j<(int) poly[i].size()/3;j++)
            gluTessVertex(tobj,&poly[i][3*j],&poly[i][3*j]);
        gluTessEndContour(tobj);
        gluTessEndPolygon(tobj);

        // draw ith polygons polyline
        glColor3f(0.0f,0.0f,0.0f);
        glBegin(GL_LINE_STRIP);
        for(int j=0;j<(int) poly[i].size()/3;j++)
            glVertex2d(poly[i][3*j+0],poly[i][3*j+1]);
        glEnd();
    }
}

void CGView::resizeGL(int width, int height) {
    glViewport(0,0,width,height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (width > height) {
        double ratio = width/(double) height;
        gluOrtho2D(-ratio,ratio,-1.0,1.0);
    }
    else {
        double ratio = height/(double) width;
        gluOrtho2D(-1.0,1.0,-ratio,ratio);
    }
    glMatrixMode (GL_MODELVIEW);
}

void CGView::worldCoord(int x, int y, double &dx, double &dy) {
    if (width() > height()) {
        dx = (2.0*x-width())/height();
        dy = 1.0-2.0*y/(double) height();
    } else {
        dx = 2.0*x/(double) width()-1.0;
        dy = (height()-2.0*y)/width();
    }
    dx /= zoom;
    dy /= zoom;
    dx += centerX;
    dy += centerY;
}

void CGView::mousePressEvent(QMouseEvent *event) {
    double dx, dy;
    worldCoord(event->x(),event->y(),dx,dy);

    // used for translation
    oldX = dx;
    oldY = dy;

    std::cout << "Mouse pressed at (" << dx << "," << dy <<")" << std::endl;

    double px_world = dx;
    double py_world = dy;
    double ux,uy,vx,vy;
    //	int i = 0;
    for(int i=(int) poly.size()-1;i>=0;i--) {
        int intersect = 0;
        double px = px_world-trans[i][0];
        double py = py_world-trans[i][1];
        px-=center[i][0];
        py-=center[i][1];
        double phi = M_PI*trans[i][2]/180.0;
        double ppx = cos(phi)*px+sin(phi)*py; // transpose
        double ppy = cos(phi)*py-sin(phi)*px;
        px=ppx; py=ppy;
        px+=center[i][0];
        py+=center[i][1];

        for(int j=0;j<(int) poly[i].size()/3-1;j++){
            ux=poly[i][3* j   +0];
            uy=poly[i][3* j   +1];
            vx=poly[i][3*(j+1)+0];
            vy=poly[i][3*(j+1)+1];

            if(vy<uy) { // swap u and v
                double t;
                t=ux;ux=vx;vx=t;
                t=uy;uy=vy;vy=t;
            }

            if(py>vy) continue;
            if(py<=uy) continue;

            if(vx*py-vy*px-ux*py+uy*px+ux*vy-uy*vx <=0){
                continue;
            }
            intersect++;


        }
        // 0=outside 1=inside
        std::cout << ((intersect%2==1)?"inside":"outside") << std::endl;
        if ((intersect%2==1)){
            i_picked = i;
            update();
            updateGL();
            return;
        }
    }

}


std::array<double, 2> CGView::getTranslatedCoords(size_t nPoly, size_t nPoint)
{
    assert(nPoly<poly.size());
    assert(nPoint<poly[nPoly].size()/3);
    double ux=poly[nPoly][3* nPoint   +0];
    double uy=poly[nPoly][3* nPoint   +1];
    //vx=poly[i][3*(j+1)+0];
    //vy=poly[i][3*(j+1)+1];
    ux-=center[nPoly][0];
    uy-=center[nPoly][1];
    double phi = M_PI*trans[nPoly][2]/180.0;
    double px = cos(phi)*ux-sin(phi)*uy;
    double py = cos(phi)*uy+sin(phi)*ux;
    px+=center[nPoly][0];
    py+=center[nPoly][1];
    px += trans[nPoly][0];
    py += trans[nPoly][1];

    return {px,py};
}

void updateSnapPoint(
        const std::array<double, 2>& p,
        const std::array<double, 2>& a,
        const std::array<double, 2>& b,
        std::array<double, 2>& o,
        double& r_min,
        double dir
        ){
/*
Seien A und B die Endpunkte der Strecke und P der zu prüfende Punkt, dann gilt:
ab = B-A ist der Vektor von A nach B
p = P-A ist der Vektor von A nach P
s = ab*p/(ab*ab) = |ab| |p| cos(ab,p) sei das skalarprodukt, normiert auf [0,1]
Ist s < 0 so liegt der Lotfußpunkt von P auf AB jenseits von A und wir ersetzen
    s=0 (und "schieben" somit den Lotfußpunkt nach A)
Ist s > 1 so liegt der Lotfußpunkt von P jenseits von A und wir ersetzen
    s = 1 (und "schieben" somit den Lotfußpunkt nach B)
    (alternativ können wir auch direkt zur nächsten Iteration springen,
    da B im nächsten Schritt zu A wird)

Mit
Q=s*ab + A erhalten wir den Punkt von AB, der P am nächsten liegt.
Der Fall Q = A (und Q=B, falls wir nicht zur nächsten Iteration springen)
ist damit direkt abgedeckt und braucht nicht gesondern überprüft zu werden.
*/
    const double& ax=a[0],
            ay=a[1],
            bx=b[0],
            by=b[1],
            px=p[0],
            py=p[1];
    double s;

    //s = ab * p /(ab*ab) = (b-a)(p-a)/((b-a)(b-a))
    s=((bx-ax)*(px-ax)+(by-ay)*(py-ay))/((bx-ax)*(bx-ax)+(by-ay)*(by-ay));

    if (s<0)
        s=0;
    if(s>1)
        return;

    double diff_x = px-s*(bx-ax)-ax;
    double diff_y = py-s*(by-ay)-ay;
    if (diff_x*diff_x+diff_y*diff_y < r_min){
        o[0] = diff_x * dir;
        o[1] = diff_y * dir;
        r_min =diff_x*diff_x+diff_y*diff_y ;
    }

}

void CGView::mouseReleaseEvent (QMouseEvent* event) {
    std::cout << "Mouse released" << std::endl;

    double r_min = 10E10;
    std::array<double, 2> o;
    for(int j=0;j<(int) poly[i_picked].size()/3-1;j++){

        for(int k=(int) poly.size()-1;k>=0;k--) {
            if (k==i_picked)
                continue;

            for(int l=0;l<(int) poly[k].size()/3-1;l++){

                updateSnapPoint(
                            getTranslatedCoords(i_picked,j),    //P
                            getTranslatedCoords(k,l),           //A
                            getTranslatedCoords(k,l+1),         //B
                            o,
                            r_min,
                            1
                            );

                updateSnapPoint(
                            getTranslatedCoords(k,l),           //P
                            getTranslatedCoords(i_picked,j),    //A
                            getTranslatedCoords(i_picked,j+1),  //B
                            o,
                            r_min,
                            -1
                            );
            }
        }
    }
    std::cout << "rmin = " << r_min << std::endl;
    if (r_min < 0.1){
        trans[i_picked][0]-=o[0];
        trans[i_picked][1]-=o[1];
    }
    updateGL();

}

void CGView::wheelEvent(QWheelEvent* event) {
    if (event->delta() < 0) zoom *= 1.1; else zoom *= 1/1.1;
    update();
}

void CGView::mouseMoveEvent (QMouseEvent* event) {
    QPoint current = event->pos();

    double dx, dy;

    worldCoord(event->x(),event->y(),dx,dy);
    trans[i_picked][0]+=(dx-oldX);
    trans[i_picked][1]+=(dy-oldY);

    //if (event->button() == Qt::LeftButton)
    oldX = dx;
    oldY = dy;
    updateGL();
}

int main (int argc, char **argv) {
    QApplication app(argc, argv);

    if (!QGLFormat::hasOpenGL()) {
        qWarning ("This system has no OpenGL support. Exiting.");
        return 1;
    }

    CGMainWindow *w = new CGMainWindow(NULL);

    w->show();

    return app.exec();
}

