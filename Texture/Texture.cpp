#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QHBoxLayout>
#include "Texture.h"

#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>



CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
: QMainWindow (parent, flags) {
	resize (604, 614);

	// Create a menu
	QMenu *file = new QMenu("&File",this);
	file->addAction ("Load picture", this, SLOT(loadPicture()), Qt::CTRL+Qt::Key_L);
    file->addAction ("Show grid", this, SLOT(showGrid()), Qt::CTRL+Qt::Key_G);
	file->addAction ("Quit", qApp, SLOT(quit()), Qt::CTRL+Qt::Key_Q);

	menuBar()->addMenu(file);

	// Create a nice frame to put around the OpenGL widget
	QFrame* f = new QFrame (this);
	f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
	f->setLineWidth(2);

	// Create our OpenGL widget
	ogl = new CGView (this,f);

	// Put the GL widget inside the frame
	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(ogl);
	layout->setMargin(0);
	f->setLayout(layout);

	setCentralWidget(f);

	statusBar()->showMessage("Ready",1000);
}

CGMainWindow::~CGMainWindow () { }

void CGMainWindow::loadPicture() {
	QString fn = QFileDialog::getOpenFileName(this, "Load picture ...", QString(), "*.jpg *.png" );

	if (fn.isEmpty()) return;
	statusBar()->showMessage ("Loading picture ...");
	QImage img(fn);

//	if (img.depth() == 8)
//		gluBuild2DMipmaps(GL_TEXTURE_2D,3,img.width(),img.height(),GL_BGRA_EXT,GL_UNSIGNED_BYTE,img.mirrored(false,true).convertToFormat(QImage::Format_RGB32).bits());

	if (img.depth() == 32)
		glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,img.width(),img.height(),0,GL_BGRA_EXT,GL_UNSIGNED_BYTE,img.mirrored(false,true).bits());

    gluBuild2DMipmaps(GL_TEXTURE_2D,3,img.width(),img.height(),GL_BGRA_EXT,GL_UNSIGNED_BYTE,img.mirrored(false,true).bits());

	ogl->updateGL();
    statusBar()->showMessage ("Loading picture done.",3000);
}

void CGMainWindow::showGrid()
{
    ogl->showGrid = !ogl->showGrid;
    ogl->update();
}

void CGMainWindow::keyPressEvent(QKeyEvent* event) {
	switch(event->key()) {
	case Qt::Key_I: std::cout << "I" << std::flush; break;
	case Qt::Key_M: std::cout << "M" << std::flush; break;
	}
	ogl->updateGL();
}

CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent) {
	main = mainwindow;


    for(size_t i=0;i<N;++i)
        for(size_t j=0;j<N;++j){
            grid[i][j][0] = 2.0*i/(N-1)-1;
            grid[i][j][1] = 2.0*j/(N-1)-1;
        }
}

void CGView::initializeGL() {

	qglClearColor(Qt::white);

	zoom = 1.0;
	centerX = 0.0;
	centerY = 0.0;

	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glPixelStorei(GL_PACK_ALIGNMENT,1);
	glGenTextures(1,&texname);
	glBindTexture(GL_TEXTURE_2D,texname);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    //glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);

//  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

//  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
}

void CGView::paintGL() {
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glScaled(zoom,zoom,1.0);
	glTranslated(-centerX,-centerY,0.0);

	glClear(GL_COLOR_BUFFER_BIT);


    if(showGrid){
        glDisable(GL_TEXTURE_2D);
        glPolygonMode(GL_FRONT, GL_LINE);

        glBegin(GL_QUADS);
        glColor3f(1.0,0.0,0.0);

        for(int i=0;i<N-1;++i)
            for(int j=0;j<N-1;++j){
                glVertex2d(grid[i][j][0],grid[i][j][1]);
                glVertex2d(grid[i+1][j][0],grid[i+1][j][1]);
                glVertex2d(grid[i+1][j+1][0],grid[i+1][j+1][1]);
                glVertex2d(grid[i][j+1][0],grid[i][j+1][1]);
            }

        glEnd();
    }
    else{
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DECAL);
        glBindTexture(GL_TEXTURE_2D,texname);
        glPolygonMode(GL_FRONT, GL_FILL);
        glBegin(GL_QUADS);

        for(int i=0;i<N-1;++i)
            for(int j=0;j<N-1;++j){
                glTexCoord2d((double)i/(N-1),(double)j/(N-1));
                glVertex2d(grid[i][j][0],grid[i][j][1]);

                glTexCoord2d((double)(i+1)/(N-1),(double)j/(N-1));
                glVertex2d(grid[i+1][j][0],grid[i+1][j][1]);

                glTexCoord2d((double)(i+1)/(N-1),(double)(j+1)/(N-1));
                glVertex2d(grid[i+1][j+1][0],grid[i+1][j+1][1]);

                glTexCoord2d((double)i/(N-1),(double)(j+1)/(N-1));
                glVertex2d(grid[i][j+1][0],grid[i][j+1][1]);
            }

        glEnd();
    }
}

void CGView::resizeGL(int width, int height) {
  glViewport(0,0,width,height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (width > height) {
    double ratio = width/(double) height;
    gluOrtho2D(-ratio,ratio,-1.0,1.0);
  }
  else {
    double ratio = height/(double) width;
    gluOrtho2D(-1.0,1.0,-ratio,ratio);
  }
  glMatrixMode (GL_MODELVIEW);
}

void CGView::worldCoord(int x, int y, double &dx, double &dy) {
  if (width() > height()) {
    dx = (2.0*x-width())/height();
    dy = 1.0-2.0*y/(double) height();
  } else {
    dx = 2.0*x/(double) width()-1.0;
    dy = (height()-2.0*y)/width();
  }
  dx /= zoom;
  dy /= zoom;
  dx += centerX;
  dy += centerY;
}

std::array<int, 2> CGView::getClosestIndex(std::array<int, 2> mousecoords)
{
    auto dist = 0.0;
    auto closestDist=std::numeric_limits<double>::max();
    auto closest = std::array<int, 2>{0,0};

    double px,py;
    worldCoord(mousecoords[0],mousecoords[1],px,py);

    for(int i = 0; i< N; ++i){
        for(int j = 0; j< N; ++j){
            dist = (grid[i][j][0]-px)*(grid[i][j][0]-px)+(grid[i][j][1]-py)*(grid[i][j][1]-py);
            if(dist < closestDist){
                closestDist=dist;
                closest = {i,j};
            }
        }
    }
    return closest;
}

/*
 * Sei P der zu verschiebende Punkt,
 * P' der verschobene Punkt und
 * Z das Zentrum der Verschiebung
 *
 * Für die streckende Funktion soll gelten:
 * P' = f(d)(P-Z) + Z (zentrische Streckung um Z mit einem Faktor, der nur vom Abstand d(P,Z) abhängt)
 * f(d)>1 für d<r (Streckung im Radius r)
 * f(d) = 1 für d>=r (Punkte außerhalb von r bleiben an ihrem Ort)
 * f(d+e) > f(d) für e>0 (Punkte die vorher weiter außen lagen liegen es hinterher immer noch)
 *      -> f(d) streng monoton steigend.
 * Möglichkeit:
 * f(d) = 1-d/2+r/2 für d<r
 *
 * Die stauchende Funktion soll die Streckung gerade umkehren, d.h.
 * f'(d') = 1/f(d)
 * f'(d') = d/d'
 * d'=d*(1-d/2+r/2)
 * d' = d -d^2/2 +dr/2
 * -d^2/2+d(1+r/2)-d' = 0
 * d^2 + d(r+2)+2d' = 0
 * d=-(r/2+1) +- sqrt((r/2+1)^2-2d')
 *
 * f'(d')(P'-Z) + Z = P
 * f'(d')f(d)(P-Z) + Z = P
 *
*/
void CGView::mousePressEvent(QMouseEvent *event) {
    double px,py;
    worldCoord(event->x(),event->y(),px,py);

    if(event->button()==Qt::RightButton){
        for(int i=0;i<N-1;++i)
            for(int j=0;j<N-1;++j){
                double gx = grid[i][j][0],
                        gy = grid[i][j][1];

                double d2 = (px-gx)*(px-gx)+(py-gy)*(py-gy);

                if(d2<r*r){
                    double f;
                    if(event->modifiers() == Qt::ControlModifier)
                        f=((1+r/2)-sqrt((r/2+1)*(r/2+1)-2*sqrt(d2)))/(sqrt(d2));
                    else
                        f = 1-sqrt(d2)/2+r/2;
                    grid[i][j][0] =  f*(gx-px) +px;
                    grid[i][j][1] =  f*(gy-py) +py;
                }

            }
    } else if(event->button()==Qt::LeftButton){
        picked = getClosestIndex({event->x(),event->y()});
        dragging = true;
        draggingCoords={px,py};
        printf("Point[%d][%d]=(%f,%f) picked\n",picked[0],picked[1],px,py);
    }
    updateGL();
}

void CGView::mouseReleaseEvent(QMouseEvent*) {
    dragging = false;
}

void CGView::wheelEvent(QWheelEvent* event) {
  double px,py,dz;
  worldCoord(event->x(),event->y(),px,py);
  if (event->delta() < 0) dz = 1.1; else dz = 1/1.1;
  zoom *= dz;
  //	centerX = px + (centerX-px)/dz;
  //	centerY = py + (centerY-py)/dz;

  update();
}

void CGView::mouseMoveEvent(QMouseEvent* event) {
    if(dragging){
        double px,py;
        worldCoord(event->x(),event->y(),px,py);
/*
    Keine Ahnung was du hier an theoretischen Überlegungen auf einem getrennten Blatt haben wolltest,
    außer dass für gauss halt gilt f(x) = e(-(x-m)^2/(2s^2)) und dass hier unser x-m eben der abstand
    zum zurzeit ausgewählten punkt ist. In dieser Implementierung nehmen wir einfach die Gitterindizes,
    aber es wären natürlich auch die aktuellen Weltkoordinaten möglich.

*/
        for(size_t i = 0; i < N; ++i)
            for(size_t j = 0; j < N; ++j){
                double f = exp(-((picked[0]-i)*(picked[0]-i)/500.0))*exp(-((picked[1]-j)*(picked[1]-j)/500.0));
                grid[i][j][0]+=f*(px-draggingCoords[0]);
                grid[i][j][1]+=f*(py-draggingCoords[1]);
            }
        draggingCoords={px,py};
    }
    updateGL();
}



int main (int argc, char **argv) {
  QApplication app(argc, argv);

  if (!QGLFormat::hasOpenGL()) {
    qWarning ("This system has no OpenGL support. Exiting.");
    return 1;
  }

  CGMainWindow *w = new CGMainWindow(NULL);

  w->show();

  return app.exec();
}

