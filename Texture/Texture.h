#ifndef TEXTURE_H
#define TEXTURE_H

#include <QMainWindow>
#include <QGLWidget>
#include <QGridLayout>
#include <QFrame>
#include <iostream>
#include <array>

#if _MSC_VER
	#include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
	#include <GL/glu.h>
#endif

class CGView;

class CGMainWindow : public QMainWindow {
	Q_OBJECT
public:
	CGMainWindow (QWidget* parent = 0, Qt::WindowFlags flags = Qt::Window);
	~CGMainWindow ();
public slots:
	void loadPicture();
    void showGrid();
protected:
	void keyPressEvent(QKeyEvent*);
private:
	CGView *ogl;
};


class CGView : public QGLWidget {
	Q_OBJECT
public:
	CGView(CGMainWindow*,QWidget*);
	void initializeGL();
	void worldCoord(int, int, double&, double&);
    std::array<int,2> getClosestIndex(std::array<int,2> mousecoords);
    std::array<int,2> picked;
    bool dragging = false;
    std::array<double,2> draggingCoords;
	double centerX,centerY,oldPosX,oldPosY,zoom;

    GLuint texname;                        // TEXTUR !

    static const size_t N = 100;
    std::array<std::array<std::array<double,2>,N>,N> grid;

    bool showGrid = false;

    double r=0.2;

protected:
	void paintGL();
	void resizeGL(int,int);
	void mouseMoveEvent(QMouseEvent*);
	void mousePressEvent(QMouseEvent*);
	void mouseReleaseEvent(QMouseEvent*);
	void wheelEvent(QWheelEvent*);
	CGMainWindow *main;
};

#endif
