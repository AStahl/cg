#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QHBoxLayout>
#include "Perspective.h"

#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>
#include <limits>

#ifdef max
#undef max
#endif

CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
    : QMainWindow (parent, flags) {
    resize (600, 600);

    // Create a menu
    QMenu *file = new QMenu("&File",this);
    file->addAction ("Load polyhedron", this, SLOT(loadPolyhedron()), Qt::CTRL+Qt::Key_L);
    file->addAction ("Quit", qApp, SLOT(quit()), Qt::CTRL+Qt::Key_Q);

    menuBar()->addMenu(file);

    // Create a nice frame to put around the OpenGL widget
    QFrame* f = new QFrame (this);
    f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
    f->setLineWidth(2);

    // Create our OpenGL widget
    ogl = new CGView (this,f);

    // Put the GL widget inside the frame
    QHBoxLayout* layout = new QHBoxLayout();
    layout->addWidget(ogl);
    layout->setMargin(0);
    f->setLayout(layout);

    setCentralWidget(f);

    statusBar()->showMessage("Ready",1000);
}

CGMainWindow::~CGMainWindow () {}

void CGMainWindow::loadPolyhedron() {
    QString filename = QFileDialog::getOpenFileName(this, "Load polyhedron ...", QString(), "OFF files (*.off)" );

    if (filename.isEmpty()) return;
    statusBar()->showMessage ("Loading polyhedron ...");
    std::ifstream file(filename.toLatin1());
    int vn,fn,en;

    ogl->min = +std::numeric_limits<double>::max();
    ogl->max = -std::numeric_limits<double>::max();

    std::string s;
    file >> s;

    file >> vn >> fn >> en;
    std::cout << "number of vertices : " << vn << std::endl;
    std::cout << "number of faces    : " << fn << std::endl;
    std::cout << "number of edges    : " << en << std::endl;

    ogl->coord.resize(vn);

    for(int i=0;i<vn;i++) {
        file >> ogl->coord[i][0] >> ogl->coord[i][1] >> ogl->coord[i][2];

        for(int j=0;j<3;++j) {
            if (ogl->coord[i][j] < ogl->min[j]) ogl->min[j] = ogl->coord[i][j];
            if (ogl->coord[i][j] > ogl->max[j]) ogl->max[j] = ogl->coord[i][j];
        }

    }

    ogl->ifs.resize(fn);

    for(int i=0;i<fn;i++) {
        int k;
        file >> k;
        ogl->ifs[i].resize(k);
        for(int j=0;j<k;j++) file >> ogl->ifs[i][j];
    }

    file.close();

    std::cout << "min = " << ogl->min << std::endl;
    std::cout << "max = " << ogl->max << std::endl;

    Vector3d extent = ogl->max - ogl->min;
    ogl->zoom = 1.5/extent.maxComp();

    ogl->center = (ogl->min+ogl->max)/2;

    ogl->updateGL();
    statusBar()->showMessage ("Loading polyhedron done.",3000);
}

void CGMainWindow::keyPressEvent(QKeyEvent* event) {
    switch(event->key()) {
    case Qt::Key_I: std::cout << "I" << std::flush; break;
    case Qt::Key_M: std::cout << "M" << std::flush; break;
    }

    ogl->updateGL();
}

CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent) {
    main = mainwindow;
}

void CGView::initializeGL() {
    qglClearColor(Qt::black);
    zoom = 1.0;
    phi = -20.7;
    theta = 19.47;
    center = 0.0;
    selectedVertex = -1;
    glEnable(GL_DEPTH_TEST); // enables Depth Test
}

void CGView::paintGL() {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0,0,5,0,0,0,0,1,0);
    glRotated(theta,1.0,0.0,0.0);
    glRotated(phi,0.0,1.0,0.0);
    glScaled(zoom,zoom,zoom);
    glTranslated(-center[0],-center[1],-center[2]);

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glColor3f(1.0,1.0,1.0);

    glLineWidth(3.0);
#if RETINA_DISPLAY
    glLineWidth(5.0);
#endif

    for(unsigned int i=0;i<ifs.size();++i) {
        glBegin(GL_LINE_LOOP);
        for(unsigned int j=0;j<ifs[i].size();++j)
            glVertex3dv(coord[ifs[i][j]].ptr());
        glEnd();
    }

    glPointSize(5.0);
#if RETINA_DISPLAY
    glPointSize(10.0);

#endif

    glBegin(GL_POINTS);
    for(unsigned int i=0;i<coord.size();++i) {
        if(i==selectedVertex)
            glColor3f(1.0,0.0,0.0);
        else
            glColor3f(1.0,1.0,1.0);
        glVertex3dv(coord[i].ptr());
    }
    glEnd();


    //just to see the points returned by rayFromPixel()
    glBegin(GL_POINTS);
        glColor3f(0.0,1.0,0.0);
        glVertex3dv(ray[0].ptr());
        glColor3f(0.0,0.0,1.0);
        glVertex3dv(ray[1].ptr());
    glEnd();
}

void CGView::resizeGL(int width, int height) {
    glViewport(0,0,width,height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (width > height) {
        double ratio = width/(double) height;
        //		glOrtho(-ratio,ratio,-1.0,1.0,-10.0,10.0);
        glFrustum(-ratio,ratio,-1.0,1.0,1.0,100.0);
    }
    else {
        double ratio = height/(double) width;
        //		glOrtho(-1.0,1.0,-ratio,ratio,-10.0,10.0);
        glFrustum(-1.0,1.0,-ratio,ratio,1.0,100.0);
    }
    glMatrixMode (GL_MODELVIEW);
}


// returns in u and v: two points that are interpolated by the ray through
// pixel (x,y) and the camera center
// u and v are IN OBJECT COORDINATES !!!
// uese projection and modelview matrix (as they are after invocation of paintGL)
void CGView::rayFromPixel(int x, int y, Vector3d &u, Vector3d &v) {
#if RETINA_DISPLAY
    x *= 2;
    y *= 2;
#endif

    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT,viewport);

    float winZ;
    float winX = (float)x;
    float winY = (float)viewport[3] - (float)y;
    glReadPixels( winX, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );

    GLdouble M[16], P[16];
    glGetDoublev(GL_PROJECTION_MATRIX,P);
    glGetDoublev(GL_MODELVIEW_MATRIX,M);

    // point on the object - if hit, or on the far plane otherwise
    gluUnProject(winX,winY,winZ,M,P,viewport,&v[0],&v[1],&v[2]);

    // punkt on the near plane (z=0)
    gluUnProject(winX,winY,0.0,M,P,viewport,&u[0],&u[1],&u[2]);


}




void CGView::mousePressEvent(QMouseEvent *event) {
    oldX = event->x();
    oldY = event->y();

    Vector3d u, v;
    rayFromPixel(event->x(), event->y(), u, v);

    std::cout << "u: " << u.x() <<" "<< u.y()<< " " << u.z() << std::endl;
    std::cout << "v: " << v.x() <<" "<< v.y()<< " " << v.z() << std::endl;

    double epsPow2 = 0.01;
    selectedVertex = findClosestVertex(u,v,epsPow2);
    updateGL();
}


// find the vertex on the model that is closer than eps to the ray and closest to the wiever
int CGView::findClosestVertex(const Vector3d u, const Vector3d v, double epsPow2)
{
    ray[0]=u;
    ray[1]=v;

    auto dist = 0.0;
    auto f = 0.0;
    auto minF = std::numeric_limits<double>::max();
    auto closest = selectedVertex;
    std::vector<size_t> closePoints;

    Vector3d euv=v-u;
    euv.normalize();

    /*
     * Längenberechnung:
     * up = p-u;
     * uv = v-u;
     * euv = uv/|uv| (Einheitsvektor entlang der Geraden)
     * up*euv = skalare Projektion von up auf uv -> gibt die Bildebene an
     * euv*(euv*up) = vektor-Projektion von up auf uv
     * euv*(euv*up) + u = nächster Punkt von p zu uv
     * (euv*(euv*up) + u)-p = vektor von p zum nächsten punkt auf der geraden
     * dessen länge ist genau die distanz
     */

    for(size_t i=0; i< coord.size(); ++i){
        f = euv*(coord[i]-u);
        dist = ((euv*f+u)-coord[i]).lengthSquared();
        if(dist < epsPow2)
            closePoints.push_back(i);
        if(dist < epsPow2 && f < minF){
            closest = i;
            minF=f;
        }
    }

    // ADD YOUR CODE HERE

    return closest;
}



void CGView::mouseReleaseEvent(QMouseEvent*) {}

void CGView::wheelEvent(QWheelEvent* event) {
    if (event->delta() < 0) zoom *= 1.2; else zoom *= 1/1.2;
    update();
}

void CGView::mouseMoveEvent(QMouseEvent* event) {
    phi += 0.2*(event->x()-oldX);
    theta += 0.2*(event->y()-oldY);
    oldX = event->x();
    oldY = event->y();

    updateGL();
}

int main (int argc, char **argv) {
    QApplication app(argc, argv);

    if (!QGLFormat::hasOpenGL()) {
        qWarning ("This system has no OpenGL support. Exiting.");
        return 1;
    }

    CGMainWindow *w = new CGMainWindow(NULL);

    w->show();

    return app.exec();
}

