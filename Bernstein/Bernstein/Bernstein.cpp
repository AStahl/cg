#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QGLWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QHBoxLayout>
#include "Bernstein.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>

#ifdef max
#undef max
#endif

/// Testfunktion, falls OpenGL nicht richtig mitspielt. Einfach am Ende jeder 
/// Funktion einfügen, als Argument den Namen der Funktion in Anführungszeichen.
void glPrintErrors(const char* as_Function)
{
  GLenum lr_Error = GL_NO_ERROR;
  bool lb_ErrorFound = false;
  int li_ErrorCount = 5;
  do
    {
      lr_Error = glGetError();
      if (lr_Error != GL_NO_ERROR)
        {
          lb_ErrorFound = true;
          li_ErrorCount--;
          char* ls_ErrorString = (char*)gluErrorString(lr_Error);
          std::cerr << "OpenGL Error (" << as_Function << "): " << ls_ErrorString << std::endl;
        }
      if (li_ErrorCount == 0)
        {
          std::cerr << "OPENGL :: ERROR Too many errors!" << std::endl;
          break;
        }
      } while (lr_Error != GL_NO_ERROR);
    if (lb_ErrorFound) exit(0);
}


CGMainWindow::CGMainWindow (QWidget* parent, Qt::WindowFlags flags)
: QMainWindow (parent, flags) {
	resize (604, 614);

	// Create a nice frame to put around the OpenGL widget
	QFrame* f = new QFrame (this);
	f->setFrameStyle(QFrame::Sunken | QFrame::Panel);
	f->setLineWidth(2);

	// Create our OpenGL widget
    ogl = new CGView (this,f);

	// Put the GL widget inside the frame
	QHBoxLayout* layout = new QHBoxLayout();
	layout->addWidget(ogl);
	layout->setMargin(0);
	f->setLayout(layout);

	setCentralWidget(f);

	statusBar()->showMessage("Ready",1000);
}

CGMainWindow::~CGMainWindow () { }

void CGMainWindow::keyPressEvent(QKeyEvent* event) {
    std::cout << "Key pressed";
	switch(event->key()) {
    case Qt::Key_Up: ogl->n++; break;
    case Qt::Key_Down: ogl->n--; break;
	}
    std::cout << ogl->n << "\n";

    ogl->updateGL();
}

CGView::CGView (CGMainWindow *mainwindow,QWidget* parent ) : QGLWidget (parent) {
	main = mainwindow;

	centerX = centerY = 0;

    n = 4;
}

void CGView::initializeGL() 
{
	qglClearColor(Qt::white);
	zoom = 1.0;
}

void CGView::paintGL() {
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glScaled(zoom,zoom,1.0);
	glTranslated(-centerX,-centerY,0.0);

	glClear(GL_COLOR_BUFFER_BIT);

    glPushMatrix();
        glColor3d(0, 0, 0);
        glScaled(1.8, 1.8, 0);
        glTranslated(-0.5, -0.5, 0);
        glBegin(GL_LINE_STRIP);
            glVertex2d(0, 1);
            glVertex2d(0, 0);
            glVertex2d(1, 0);
            glVertex2d(1, 1);
        glEnd();

    std::cout << "Berechne fuer n=" << n << " ...";
    if (n > 0) {
        std::vector<Vector3d> farbe = colorV(n+1);
        for (int i = 0; i <= n; i++) {
            glColor3dv(((Vector3d)farbe[i]).ptr());
            glBegin(GL_LINE_STRIP);
            for (double t = 0; t <= 1; t+=0.001) {
                glVertex2d(t, B(i, t));
            }
            glEnd();
        }
        std::cout << " Fertig!" << std::endl;
    }
    else std::cout << " Fehler!" << std::endl;


    glPopMatrix();
}

void CGView::resizeGL(int width, int height) {
	glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width > height) {
		double ratio = width/(double) height;
		gluOrtho2D(-ratio,ratio,-1.0,1.0);
	}
	else {
		double ratio = height/(double) width;
		gluOrtho2D(-1.0,1.0,-ratio,ratio);
	}
	glMatrixMode (GL_MODELVIEW);
}

std::vector<Vector3d> CGView::colorV(int anzahl) {
    std::vector<Vector3d> res;
    Vector3d stdFarben[] = {Vector3d(1,0,0), Vector3d(0,1,0), Vector3d(0,0,1), Vector3d(1,1,0), Vector3d(1,0,1), Vector3d(0,1,1)};
    int j = 0;
    for (int i = 0; i < anzahl; i++) {
        if (i%6==0) j++;
        res.push_back(stdFarben[i%6]/j);
    }
    std::cout << j;
    return res;
}

double CGView::B(int i, double t) {
    return binCoeff(n, i) * pow(t, i) * pow((1-t), (n -i));
}

int main (int argc, char **argv) {
	QApplication app(argc, argv);

	if (!QGLFormat::hasOpenGL()) {
		qWarning ("This system has no OpenGL support. Exiting.");
		return 1;
	}

	CGMainWindow *w = new CGMainWindow(NULL);

	w->show();

    std::cout << "Mit Pfeiltasten hoch und runter n anpassen" << std::endl;

	return app.exec();
}

