#ifndef HELLOQGL_H
#define HELLOQGL_H

#include <QMainWindow>
#include <QGLWidget>
#include <QGridLayout>
#include <QFrame>
#include <vector>

#if _MSC_VER
	#include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
	#include <GL/glu.h>
#endif
#include "vecmath.h"

#ifndef VECMATH_VERSION
#error "wrong vecmath included, must contain a VECMATH_VERSION macro"
#else
#if VECMATH_VERSION < 2
#error "outdatet vecmath included"
#endif
#endif

class CGView;

class CGMainWindow : public QMainWindow {
	Q_OBJECT

public:

	CGMainWindow (QWidget* parent = 0, Qt::WindowFlags flags = Qt::Window);
	~CGMainWindow ();

public:


protected:
    void keyPressEvent(QKeyEvent*);


private:

	CGView *ogl;
};

class CGView : public QGLWidget {
	Q_OBJECT

public:

	CGView(CGMainWindow*,QWidget*);
	void initializeGL();
    std::vector<Vector3d> colorV(int n);
    double B(int i, double t);
    double minX,minY,maxX,maxY;
	double centerX,centerY,zoom;

    int n;

    Vector3d color;

    int binCoeff(int n, int k) {
        if (k == 0 || k == n) return 1;
        return (binCoeff(n-1, k) + binCoeff(n-1, k-1));
    }


public slots:

protected:

	void paintGL();
    void resizeGL(int,int);

	CGMainWindow *main;
};

#endif

