#ifndef HELLOWORLD_H
#define HELLOWORLD_H

#include <QGLWidget>



#if _MSC_VER
    #include <gl/glu.h>
#elif __APPLE__
  #include <OpenGL/glu.h>
#else
    #include <GL/glu.h>
#endif


class HelloWorld : public QGLWidget {

private:
	void printMatrix(GLint M);
public:

	HelloWorld() {
	}

	QSize sizeHint() const {
		return QSize(500,500);
	}

protected:
	void initializeGL();
	void paintGL();
	void resizeGL(int width, int height);
};
#endif // HELLOWORLD_H
