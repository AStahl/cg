#include <iostream>
#include "helloworld.h"

#define WIDTH 100
#define HEIGHT 100

void HelloWorld::initializeGL() {
	glClearColor(1.0f,1.0f,1.0f,1.0f);
}

void HelloWorld::paintGL() {

	glClear(GL_COLOR_BUFFER_BIT);
	//glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// View Transform
	gluLookAt(0,0,1, 0,0,0, 0.1,1,0); 

	// Model Transform
	glScalef(0.8f,0.8f,1.0f);	
    glScaled(0.8,0.8,1.0);
    glRotated(20, 0,0,1);
    glTranslated(0.4,0,0);
	
	std::cout << "Model-View Transform Matrix: " << std::endl;	
	printMatrix(GL_MODELVIEW_MATRIX);

	glColor3f(1.0f,0.0f,0.0f);
	glBegin(GL_LINES);
        // G
	glVertex2f(-0.25f,1.0f); // Objektkoordinaten
	glVertex2f(-1.0f, 1.0f);
	glVertex2f(-1.0f,-1.0f);
	glVertex2f(-1.0f, 1.0f);
	glVertex2f(-1.0f,-1.0f);
	glVertex2f( -0.25f, -1.0f);
	glVertex2f( -0.25f, -1.0f);
	glVertex2f( -0.25f, 0.0f);
	glVertex2f( -0.25f, 0.0f); 
	glVertex2f( -0.5f, 0.0f);
        // L
	glVertex2f( 0.0f,-1.0f);
	glVertex2f( 1.0f, -1.0f);
	glVertex2f( 0.0f,-1.0f);
	glVertex2f( 0.0f, 1.0f);
	glEnd();

}

void HelloWorld::resizeGL(int width, int height) {

    double f = (width/WIDTH<height/HEIGHT)?width/WIDTH:height/HEIGHT;

    glViewport((width-f*WIDTH)/2,(height-f*HEIGHT)/2,f*WIDTH,f*HEIGHT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-1,1,-1,1);
	std::cout << "Projection Matrix: " << std::endl;	
	printMatrix(GL_PROJECTION_MATRIX);
	glMatrixMode (GL_MODELVIEW);
}




// prints OpenGL Matrix M 
void HelloWorld::printMatrix(GLint M){
	// Values for M: 
	// GL_MODELVIEW_MATRIX
	// GL_PROJECTION_MATRIX
	// GL_TEXTURE_MATIRX
	GLfloat matrix[16]; 
	glGetFloatv (M, matrix);
	for (int i=0;i<4;i++){
		for (int j=0;j<4;j++)
			// OpenGL matrices are column first order!
			std::cout << matrix[j*4+i] << " ";
		std::cout << std::endl;
	}

}














