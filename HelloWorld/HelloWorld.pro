TEMPLATE = app 		# an application is to be build
TARGET = HelloWorld 	# name of application
QT += gui opengl 	# QT modules to be used
CONFIG += console 	# a console for text output is opened together with the application

# header and source files, wild card expressions like *.h / *.cpp can also be used

HEADERS += helloworld.h
SOURCES += helloworld.cpp main.cpp


macx: QMAKE_MAC_SDK =  macosx10.11
unix:!macx: LIBS+= -lGLU
